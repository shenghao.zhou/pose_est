from pytorch3d.renderer import (
    RasterizationSettings,
    MeshRenderer,
    MeshRasterizer,
    HardPhongShader,
    SoftSilhouetteShader,
    BlendParams
)

from pytorch3d.io import load_obj
import torch
import pytorch3d
import matplotlib.pyplot as plt
import numpy as np

def get_device():
    """
    Checks if GPU is available and returns device accordingly.
    """
    if torch.cuda.is_available():
        device = torch.device("cuda:0")
    else:
        device = torch.device("cpu")
    return device


def get_mesh_renderer(image_size=512, lights=None, device=None):
    """
    Returns a Pytorch3D Mesh Renderer.

    Args:
        image_size (int): The rendered image size.
        lights: A default Pytorch3D lights object.
        device (torch.device): The torch device to use (CPU or GPU). If not specified,
            will automatically use GPU if available, otherwise CPU.
    """
    if device is None:
        device = get_device()
    raster_settings = RasterizationSettings(
        image_size=image_size, blur_radius=0.0, faces_per_pixel=1,
        bin_size=500,
        max_faces_per_bin=1000000,
    )
    renderer = MeshRenderer(
        rasterizer=MeshRasterizer(raster_settings=raster_settings),
        shader=HardPhongShader(device=device, lights=lights),
    )
    return renderer

def get_silhouette_renderer(image_size=512, lights=None, device=None):
    """
    Returns a Pytorch3D Mesh Renderer.

    Args:
        image_size (int): The rendered image size.
        lights: A default Pytorch3D lights object.
        device (torch.device): The torch device to use (CPU or GPU). If not specified,
            will automatically use GPU if available, otherwise CPU.
    """
    blend_params = BlendParams(sigma=1e-4, gamma=1e-4)
    if device is None:
        device = get_device()
    raster_settings_silhouette = RasterizationSettings(
        image_size=image_size, 
        blur_radius=0.0, #np.log(1. / 1e-4 - 1.)*blend_params.sigma, 
        faces_per_pixel=50, 
        bin_size=100,
        max_faces_per_bin=1000000,
    )
    renderer = MeshRenderer(
        rasterizer=MeshRasterizer(raster_settings=raster_settings_silhouette),
        shader=SoftSilhouetteShader(),
    )
    return renderer


def load_mesh(path):
    """
    Loads vertices and faces from an obj file.

    Returns:
        vertices (torch.Tensor): The vertices of the mesh (N_v, 3).
        faces (torch.Tensor): The faces of the mesh (N_f, 3).
    """
    vertices, faces, _ = load_obj(path)
    faces = faces.verts_idx
    return vertices, faces

def to_batch_tensor(data, ndim):
    data = torch.tensor(data)
    if len(data.shape) < ndim:
        data = data.unsqueeze(0)
        
        assert len(data.shape) == ndim
    else:
        assert data.shape[0] == 1
    return data

def render(path, R, image_size=256, device=None):
    if device is None:
        device = get_device()
    
    # Get the renderer.
    renderer = get_mesh_renderer(image_size=image_size)

    # Get the vertices, faces, and textures.
    color = [0.7, 0.7, 1]
    vertices, faces = load_mesh(path)
    vertices = vertices.unsqueeze(0)  # (N_v, 3) -> (1, N_v, 3)
    faces = faces.unsqueeze(0)  # (N_f, 3) -> (1, N_f, 3)
    textures = torch.ones_like(vertices)  # (1, N_v, 3)
    textures = textures * torch.tensor(color)  # (1, N_v, 3)
    mesh = pytorch3d.structures.Meshes(
        verts=vertices,
        faces=faces,
        textures=pytorch3d.renderer.TexturesVertex(textures),
    )
    
    # mesh = load_obj(path)
    mesh = mesh.to(device)
    
    dist = 1000
    
    # Prepare the camera:
    cameras = pytorch3d.renderer.FoVPerspectiveCameras(
        R=R.unsqueeze(0), T=torch.tensor([[0, 0, dist]]), fov=60, device=device
    )

    # Place a point light in front of the cow.
    lights = pytorch3d.renderer.PointLights(location=[[0, 0, -3]], device=device)

    rend = renderer(mesh, cameras=cameras, lights=lights)
    rend = rend.cpu().numpy()[0, ..., :3]  # (B, H, W, 4) -> (H, W, 3)
    # The .cpu moves the tensor to GPU (if needed).
    return rend



if __name__ == '__main__':
    R = pytorch3d.transforms.euler_angles_to_matrix(
        torch.tensor([np.pi/2, np.pi/4, 0]), "XYZ"
    )
    img = render('station_new.obj', R)
    # plt.imshow(img)
    # plt.show()
    plt.imsave('res.jpg', img)
    