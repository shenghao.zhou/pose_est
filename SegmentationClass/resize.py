import cv2
import argparse


parser = argparse.ArgumentParser()
parser.add_argument('img_name')
parser.add_argument('--ratio', type=float, default=0.25)
args = parser.parse_args()
img = cv2.imread(args.img_name)
H, W = img.shape[:2]
resize_img = cv2.resize(img, (int(W * args.ratio), int(H * args.ratio)), cv2.INTER_CUBIC)
cv2.imwrite('resize_'+args.img_name, resize_img)
