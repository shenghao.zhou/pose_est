import torch.nn as nn
import torch
from tqdm import tqdm 
import imageio
import matplotlib.pyplot as plt
from skimage import img_as_ubyte
import numpy as np
import utils 
import torch.nn.functional as F
from PIL import Image 
from pytorch3d.structures import Meshes
import pytorch3d 
import pytorch3d.transforms
class Model(nn.Module):
    def __init__(self, mesh, renderer, img_ref, cam):
        super().__init__()
        self.mesh = mesh
        self.renderer = renderer
        img_ref = torch.from_numpy(img_ref[None, :].astype(np.float32))
        
        
        self.register_buffer("img_ref", img_ref)
        # self.cam_pos = nn.Parameter(
        #     torch.tensor([])
        # )
        # R_init_mat = torch.tensor([[0., 0, -1],
        #                                     [-1, 0, 0],
        #                                     [0, 1, 0]], device=mesh.device).unsqueeze(dim=0)
        R_init_quaternion = torch.tensor([0.3536, 0.6124, 0.6124, 0.3536], device=mesh.device).unsqueeze(dim=0)
        R_init_6d = pytorch3d.transforms.matrix_to_rotation_6d(
            pytorch3d.transforms.quaternion_to_matrix(R_init_quaternion)
        )
        
        self.R = nn.Parameter(R_init_6d)
        self.t = nn.Parameter(torch.tensor([[0., 0, 2000]], device=mesh.device))
        self.cam = cam
    
    def forward(self):
        R = pytorch3d.transforms.rotation_6d_to_matrix(self.R)
        t = self.t
        img = self.renderer(meshes_world=self.mesh.clone(), R=R, T=t, cameras=self.cam)[..., 3]
        loss = F.mse_loss(img, self.img_ref, reduction='sum')
        return loss, img 
    

def load_ref_img(img_name):
    img_path = "./SegmentationClass/{}".format(img_name)
    return np.array(Image.open(img_path).convert('L')) > 0


def main():
    
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('--lr', type=float, default=5e-1)
    parser.add_argument('--img_name', type=str, default='resize_1m.png')

    args = parser.parse_args()
    
    img_name = args.img_name
    img_size = [1080 // 4, 1920 // 4]
    
    mesh_path = 'station_new.obj'
    img_ref = load_ref_img(img_name)
    
    
    lr = args.lr
    outdir = 'out_{}_lr_{}'.format(img_name, str(lr))
    import os
    os.makedirs(outdir, exist_ok=True)
    
    
    
    device = utils.get_device()
    vertices, faces = utils.load_mesh(mesh_path)
    vertices = vertices.unsqueeze(0).to(device)  # (N_v, 3) -> (1, N_v, 3)
    faces = faces.unsqueeze(0).to(device)  # (N_f, 3) -> (1, N_f, 3)
    textures = torch.ones_like(vertices)
    mesh = Meshes(
        verts=vertices,
        faces=faces,
        textures=pytorch3d.renderer.TexturesVertex(textures), 
    )
    
    cameras = pytorch3d.renderer.FoVPerspectiveCameras(device=device,)
    
    silhouette_renderer = utils.get_silhouette_renderer(img_size)
    vis_renderer = utils.get_mesh_renderer(img_size)
    
    
    model = Model(mesh, silhouette_renderer, img_ref, cameras).to(device)
    optimizer = torch.optim.Adam(model.parameters(), lr=lr)
    n_iter = 10000
    loop = tqdm(range(n_iter))
    

    
    plt.figure(figsize=(10, 10))

    _, image_init = model()
    plt.subplot(1, 2, 1)
    plt.imshow(image_init.detach().squeeze().cpu().numpy())
    plt.grid(False)
    plt.title("Starting position")

    plt.subplot(1, 2, 2)
    plt.imshow(model.img_ref.cpu().numpy().squeeze())
    plt.grid(False)
    plt.title("Reference silhouette")
    
    plt.savefig('res.png', dpi=800)
    # import pdb; pdb.set_trace() # yapf:disable
    
    
    filename_output = "{}/demo.gif".format(outdir)
    # writer = imageio.get_writer(filename_output, mode='I', fps=5)
    imgs = []
    for i in loop:
        optimizer.zero_grad()
        loss, _ = model()
        loss.backward()
        optimizer.step()
        loop.set_description('Optimizing (loss %.4f)' % loss.data)
        if loss.item() < 200:
            break
    
        # Save outputs to create a GIF. 
        if i % 50 == 0:
            # R = look_at_rotation(model.camera_position[None, :], device=model.device)
            # T = -torch.bmm(R.transpose(1, 2), model.camera_position[None, :, None])[:, :, 0]   # (1, 3)
            R = pytorch3d.transforms.rotation_6d_to_matrix(model.R)
            t = model.t
            print(R)
            print(t)
            image = vis_renderer(meshes_world=model.mesh.clone(), R=R, T=t, cameras=cameras)
            image = image[0, ..., :3].detach().squeeze().cpu().numpy()
            
            image = img_as_ubyte(image)
            # writer.append_data(image)
            # imgs.append(image)
            
            # plt.figure()
            # plt.imshow(image[..., :3])
            # plt.title("iter: %d, loss: %0.2f" % (i, loss.data))
            # plt.axis("off")
            # plt.show()    
            plt.imsave('{}/{:05d}.jpg'.format(outdir, i), image[..., :3])
    
    # imageio.mimsave(filename_output, imgs, duration=3)
    print(torch.save(model.R.detach(), '{}/R.pth'.format(outdir)))
    print(torch.save(model.t.detach(), '{}/t.pth'.format(outdir)))

if __name__ == "__main__":
    main()