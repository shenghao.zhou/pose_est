import imageio.v2 as iio
from pathlib import Path

imgs = sorted(Path('.').glob('*.jpg'))
w = iio.get_writer('my_video.mp4', format='FFMPEG', mode='I', fps=3,)

for img_path in imgs:
    img = iio.imread(img_path)
    w.append_data(img)
w.close()
