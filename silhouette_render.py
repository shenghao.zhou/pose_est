from utils import get_device, get_mesh_renderer, load_mesh, to_batch_tensor, get_silhouette_renderer
import torch 
import pytorch3d
import matplotlib.pyplot as plt
import numpy as np 

def render(path, R, t, image_size=256, device=None):
    if device is None:
        device = get_device()
    
    # Get the renderer.
    renderer = get_silhouette_renderer(image_size=image_size)

    # Get the vertices, faces, and textures.
    color = [0.7, 0.7, 1]
    vertices, faces = load_mesh(path)
    vertices = vertices.unsqueeze(0)  # (N_v, 3) -> (1, N_v, 3)
    faces = faces.unsqueeze(0)  # (N_f, 3) -> (1, N_f, 3)
    textures = torch.ones_like(vertices)  # (1, N_v, 3)
    textures = textures * torch.tensor(color)  # (1, N_v, 3)
    mesh = pytorch3d.structures.Meshes(
        verts=vertices,
        faces=faces,
        textures=pytorch3d.renderer.TexturesVertex(textures),
    )
    
    # mesh = load_obj(path)
    mesh = mesh.to(device)
    
    R = to_batch_tensor(R, ndim=3) # (1, 3, 3)
    t = to_batch_tensor(t, ndim=2) # (1, 3)
    
    # Prepare the camera:
    cameras = pytorch3d.renderer.FoVPerspectiveCameras(
        R=R, T=t, fov=60, device=device
    )

    # Place a point light in front of the cow.
    lights = pytorch3d.renderer.PointLights(location=[[0, 0, -3]], device=device)

    rend = renderer(mesh, cameras=cameras, lights=lights)
    # rend = rend.cpu().numpy()[0, ..., :3]  # (B, H, W, 4) -> (H, W, 3)
    # The .cpu moves the tensor to GPU (if needed).
    return rend


def world2render(R, t):
    if len(t.shape) == 1:
        t = t.reshape(1, -1)
    # print(R, t)
    new_R = np.array([[0, -1, 0],
                      [0, 0, 1],
                      [-1, 0, 0]])
    R = R @ new_R.T
    # R = new_R.T @ R
    t = -t @ new_R.T
    return R, t

if __name__ == '__main__':
    theta = 0 # np.pi / 2
    R = np.array([[np.cos(theta), -np.sin(theta), 0],
                  [np.sin(theta), np.cos(theta),  0],
                  [0            , 0,              1]])
    # t = np.array([0, 0, 1000])
    t = np.array([1000, 0, 0])

    R, t = world2render(R, t)
    mask = render('station_new.obj', R, t, image_size=[1080, 1920])
    # import pdb; pdb.set_trace() # yapf:disable
    mask = mask[0, :, :, 3].cpu().numpy()
    plt.imsave('res2.png', mask)